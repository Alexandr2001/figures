package ru.vaa.figures;

public class Square extends Shape {
    private Point center;

    private double side;

    Square(Color color, Point center, double side){
        super(color);
        this.center = center;
        this.side = side;
    }

    public double area(){
        return side * side;
    }

    @Override
    public String toString() {
        return "\nквадрат{" +
                "\nцентер: " + center +
                "\nсторона: " + side +
                '}';
    }
}

