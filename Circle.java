package ru.vaa.figures;

public class Circle extends Shape {
    private Point center;

    private double radius;

    Circle(Color color, Point center, double radius){
        super(color);
        this.center = center;
        this.radius = radius;
    }

    public double area() {
        return  Math.PI * radius * radius;
    }

    @Override
    public String toString() {
        return  "\nКруг {" +
                "\nцвет: " + getColor() +
                "\nцентер: " + center +
                "\nрадиус: " + radius +
                "}";
    }
}
